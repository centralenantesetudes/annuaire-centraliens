const decorate = require('fetch-cookie/node-fetch');
const Rx = require('rxjs/Rx');
const querystring = require('querystring');
const libxmljs = require('libxmljs');
const fetch = decorate(require('node-fetch'));
const fp = Rx.Observable.fromPromise;

const firstPage = 'https://annuaire.centraliens-nantes.net';
const loginReq = 'https://annuaire.centraliens-nantes.net/index.php/login';
const searchPage = 'https://annuaire.centraliens-nantes.net/index.php/annuaire/search';
const indivPage = 'https://annuaire.centraliens-nantes.net/index.php/annuaire/show/individu_id/';


exports.login = (login, pass) => {
	let body = querystring.stringify({
		'signin[username]': login,
		'signin[password]': pass
	});
	let headers = {
		'content-type': 'application/x-www-form-urlencoded',
		'content-length': body.length,
		'user-agent': ''
	};
	return fp(fetch(firstPage))
		.flatMap(_ => fp(fetch(loginReq, { method: 'POST', body: body, headers: headers })));
};

const extractIds = doc => doc.find('//div[@id="content"]//table[@class="striped"]//a')
	.map(a => a.attr('href').value().split('/').pop());

const getLastPage = doc => doc
	.find('//div[@id="content"]//div[@class="center paginate"][1]/a[last()]')
	.map(last => last.attr('href').value().split('/').pop())
	.concat(0)
	.shift();

const queryMapper = {
	nom: 'individu_nom',
	prenom: 'individu_prenom',
	promo1: 'promotion_debut',
	promo2: 'promotion_fin',
	entreprise: 'entreprise_nom'
}

const buildQuery = (query, page) => searchPage + '?' + querystring.stringify(Object.keys(query).reduce((acc, key) => {
	acc['recherche['+(queryMapper[key] || key)+']'] = query[key];
	return acc;
}, {commit: 'Rechercher', page: page}));

const getQueryPaged = (query, page, last) => fp(fetch(buildQuery(query, page)))
	.flatMap(res => fp(res.text()))
	// .do(console.log)
	.map(libxmljs.parseHtmlString)
	.flatMap(doc => {
		last = typeof last == 'undefined' ? parseInt(getLastPage(doc), 10) : last;
		let next = page + 1;
		return Rx.Observable.from(extractIds(doc))
			.concat(next <= last ? getQueryPaged(query, next, last) : Rx.Observable.empty());
	});

const getPromo = promo => getQueryPaged({promo1: promo, promo2: promo}, 1);

const jobReg = /^(.+) \([0-9]{4}[A-Z]\) - \((.+) à (.+)\)$/i;
const mailReg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
const phoneReg = /^((?:[0-9]{2}[-\.]?){5})(?: \([\w ]+\))?$/i;

const parseJob = job => {
	job.find('br').map(br => br.replace('\n'));
	let splitted = job.text().split('\n').map(s => s.trim());
	splitted.shift();
	let company = (jobReg.exec(splitted.shift()) || ['', ''])[1];
	let emails = splitted
		.map(frag => mailReg.exec(frag))
		.filter(res => res != null)
		.map(res => res[0]);
	let phones = splitted
		.map(frag => phoneReg.exec(frag))
		.filter(res => res != null)
		.map(res => res[1]);
	return {company, emails, phones};
}

const flatten = arr => arr.reduce((acc, cur) => acc.concat(cur), []);
const trimInner = str => str.split(' ')
	.map(s => s.trim())
	.filter(s => s.length > 0)
	.join(' ');

exports.getIds = query => getQueryPaged(query, 1);

exports.getMeta = id => ({id: id, url: indivPage + id, source: 'alumnis'});

exports.getDetails = meta => fp(fetch(indivPage + meta.id))
	.flatMap(res => fp(res.text()))
	.map(libxmljs.parseHtmlString)
	.map(doc => {
		let ident = doc.find('//div[@class="typography"]//td')
			.map(td => td.text())
			.map(trimInner);
		let diplomes = doc.find('//div[@class="typography"]//fieldset[2]//li')
			.map(li => li.text());
		let coords = doc.find('//div[@id="coords"]/div');
		let nthCoord = i => coords[i].find('.//dd').map(dd => dd.text().trim());
		let jobs = doc.find('//div[@class="typography"]//fieldset[4]/ul/li')
			.map(parseJob);
		return {
			...meta,
			nom: ident[0].trim(),
			prenom: ident[1].trim(),
			statut: ident[2].trim(),
			diplomes: diplomes,
			adresses: nthCoord(0),
			numeros: nthCoord(1).concat(flatten(jobs.map(j => j.phones))),
			emails: nthCoord(2).concat(flatten(jobs.map(j => j.emails))),
			entreprise: jobs.length > 0 ? jobs.shift().company : '',
			ecole: 'Nantes'
		};
	});