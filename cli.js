const Rx = require('rxjs/Rx');
const fs = require('fs');

exports.getAuthDetails = () => Rx.Observable.create((observer) => {
	if (!fs.existsSync('.auth')) {
		observer.complete();
		return;
	}
	fs.readFile('.auth', 'utf8', (err, data) => {
		if (err) {
			observer.complete();
			return;
		}
		observer.next(data.split('\n').reduce((acc, line) => {
			let splitted = line.split(':');
			acc[splitted[0]] = [splitted[1], splitted[2]];
			return acc;
		}, {}));
	});
});

exports.getAuthFromCli = (sources, users, passwords) => sources.reduce((acc, src, i) => {
	if (users[i] != null && passwords[i] != null) {
		acc[src] = [users[i], passwords[i]];
	}
	return acc;
}, {});