const crypto = require('crypto-js');
const decorate = require('fetch-cookie');
const Rx = require('rxjs/Rx');
const FormData = require('form-data');
const libxmljs = require('libxmljs');
const fetch = decorate(require('node-fetch'));
const fp = Rx.Observable.fromPromise;

const loginPage = 'https://www.centrale-carrieres.com/err/403.phtml';
const loginReq = 'https://www.centrale-carrieres.com/login.php';
const annuaire = 'https://www.centrale-carrieres.com/annuaire/index.php';
const personne = 'https://www.centrale-carrieres.com/annuaire/view.php';

const ssha512 = (cleartext, salt) => {
	salt = typeof salt == 'undefined' ? crypto.lib.WordArray.random(4) : crypto.enc.Hex.parse(salt);
	let plain = crypto.enc.Utf8.parse(cleartext);
	let digest = crypto.SHA512(plain.concat(salt));
	let ssha = '{SSHA512}' + digest.concat(salt).toString(crypto.enc.Base64);
	return ssha;
};

const loginForm = (challenge, acct_name, acct_pass) => {

	let str = acct_name.toUpperCase() + ':' +
		crypto.MD5(acct_pass).toString() + ':' +
		challenge;

	let sha512 = ssha512(acct_pass, challenge.substr(0,8));
	let response = crypto.MD5(str).toString();

	let form = new FormData();
	form.append('challenge', challenge);
	form.append('sha512', sha512);
	form.append('response', response);
	form.append('redirect', '/annuaire/index.php');
	form.append('acct_name', acct_name);
	form.append('acct_pass', '');
	return form;
};

const queryForm = query => {
	query = {
		nom: '',
		prenom: '',
		statut: 1,
		ville: '',
		pays: 1,
		promo1: '',
		promo2: '',
		entreprise: '',
		...query
	};
	let form = new FormData();
	form.append('nom', query.nom);
	form.append('prenom', query.prenom);
	form.append('statut', query.statut);
	form.append('perso_ville', query.ville);
	form.append('perso_pays', query.pays);
	form.append('pro_fonction_desc', '');
	form.append('pro_organisation', query.entreprise);
	form.append('type', 1);
	form.append('promo1', parseInt(query.promo1, 10));
	form.append('promo2', parseInt(query.promo2, 10));
	form.append('Rechercher', 'Rechercher');
	return form;
};

const getChallenge = doc => doc.get('//input[@name="challenge"]');

exports.login = (name, pass) => fp(fetch(loginPage))
	.flatMap(res => fp(res.text()))
	.map(libxmljs.parseHtmlString)
	.map(getChallenge)
	.map(input => loginForm(input.attr('value').value(), name, pass))
	.flatMap(form => fp(fetch(loginReq, { method: 'POST', body: form, headers: form.getHeaders() })));

const getLastStart = doc => {
	return doc.find('//input[@title="Fin"]')
		.map(last => last.attr('onclick').value().split('start=').pop())
		.map(last => parseInt(last.substr(0, last.length - 1), 10))
		.concat(0)
		.shift();
};

const getCentraliens = doc => doc.find('//tr[@class="Tmpl_Table_List0" or @class="Tmpl_Table_List1"]/td/a')
	.map(a => a.attr('href').value().split('id=').pop());

const getQueryPaged = (query, start, last) => {
	let form = queryForm(query);
	return fp(fetch(annuaire + '?start=' + start, { method: 'POST', body: form, headers: form.getHeaders() }))
		.flatMap(res => fp(res.text()))
		.map(libxmljs.parseHtmlString)
		.flatMap(doc => {
			last = typeof last == 'undefined' ? getLastStart(doc) : last;
			let next = start + 50;
			return Rx.Observable.from(getCentraliens(doc))
				.concat(next <= last ? getQueryPaged(query, next, last) : Rx.Observable.empty());
		});
};

const getPromo = promo => getQueryPaged({promo1: promo, promo2: promo}, 0);

const getCoords = doc => doc.find('//table[@class="Tmpl_Table_Fiche"]//table//td')
	.map(td => td.text())
	.map(td => td.split('\n')
		.map(l => l.trim())
		.filter(l => l.length > 0)
		.join('\n'))
	.filter(td => td.length > 0);

const split2 = (str, sep, lim) => {
	arr = str.split(sep);
	res = arr.splice(0, lim);
	res.push(arr.join(sep));
	return res;
};

const populateRes = (res, line) => {
	let s = split2(line, ':', 1).map(s => s.trim());
	let k = s.shift()
		.toLowerCase()
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, '')
		.replace(/\s/g, '_')
		.replace(/\./g, '');
	let v = s.shift();
	let newKey = typeof res[k] == 'undefined' || res[k] == v; 
	return {...res, [k]: newKey ? v : res[k] + ',' + v };
};

const reg = /^((?:M.|Mme) )?(.+) (.+) \(Centralien de (\w+) \/ ([0-9]{4}) \)$/i;

const formatCoords = coords => {
	if (coords.length == 0) return {};
	let mainCoords = coords.shift().split('\n').slice(0, 3).join(' ').trim();
	let exec = reg.exec(mainCoords);
	let res = {
		sexe: exec[1] || '?',
		prenom: exec[2],
		nom: exec[3],
		ecole: exec[4],
		promo: exec[5],
	};
	res = coords.map(c => c.split('\n'))
		.reduce((res, lines) => {
			return lines.filter(l => l.indexOf(':') > -1)
				.reduce(populateRes, res);
		}, res);
	res.emails = res.courriel;
	res.numeros = [res.mob, res.tel].join(',');
	res.entreprise = res.organisation;
	return res;
};

exports.getIds = q => getQueryPaged(q, 0);

exports.getMeta = id => ({id: id, url: personne + '?id=' + id, source: 'cc'});

exports.getDetails = meta => fp(fetch(personne + '?id=' + meta.id))
	.flatMap(res => fp(res.text()))
	.map(libxmljs.parseHtmlString)
	.map(getCoords)
	.map(formatCoords)
	.map(details => ({...details, ...meta}));