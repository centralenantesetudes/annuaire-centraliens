#!/usr/bin/env node

const cli = require('./cli');
const Rx = require('rxjs/Rx');
const program = require('commander');

let sources = {
	cc: require('./centrale-carrieres'),
	alumnis: require('./alumnis')
};

function withCredentials(sources, users, passwords) {
	let cliCred = cli.getAuthFromCli(sources, users, passwords);
	return cli.getAuthDetails()
		.map(fileCred => Object.keys(fileCred).reduce((filtered, key) => {
			if (sources.indexOf(key) > -1) {
				filtered[key] = fileCred[key];
			}
			return filtered;
		}, {}))
		.map(fileCred => Object.assign({}, fileCred, cliCred));
}

function readFromSource(credentials, query, number, onlyMeta) {
	return Rx.Observable.from(Object.keys(credentials))
		.flatMap(src => {
			let actual = sources[src];
			let [login, pass] = credentials[src];
			return actual.login(login, pass)
				.flatMap(_ => actual.getIds(query))
				.take(number)
				.map(actual.getMeta)
				.flatMap(meta => onlyMeta ? Rx.Observable.of(meta) : actual.getDetails(meta))
		});
}

function parseQuery(s) {
	return s.split(',').reduce((acc, curr) => {
		let [k, v] = curr.split('=');
		if (k === 'promo') {
			let vs = v.split('-');
			acc['promo1'] = vs[0];
			acc['promo2'] = vs[vs.length-1];
		} else {
			acc[k] = v;
		}
		return acc;
	}, {});
}

program
	.version('0.1')
	.command('k1=v1[,k2=v2...]', 'recherche')
	.option('-n, --number <n>', 'nombre de résultats', parseInt)
	.option('-s, --source <list>', 'sources de données', v => v.split(','))
	.option('-u, --user <list>', 'utilisateur', v => v.split(','))
	.option('-p, --password <list>', 'mot de passe', v => v.split(','))
	.option('-c, --cols <list>', 'colonnes à afficher', v => v.split(','))
	.option('-m, --meta', 'renvoyer les métadonnées uniquement')
	.action((query, cmd) => {
		let sources = cmd.source || [];
		let number = cmd.number != null ? cmd.number : 10;
		let cols = (cmd.cols || ['source', 'id', 'url']);
		let str = cols.map(s => s.padEnd(20)).join('|');
		console.log(str);
		console.log(Array.from(str).map(s => '-').join(''));
		withCredentials(sources, cmd.user || [], cmd.password || [])
			.flatMap(credentials => readFromSource(credentials, parseQuery(query), number, cmd.meta || false))
			.map(details => cols.reduce((final, col) => {
				return final.concat([details[col] != null ? details[col] : '']);
			}, []))
			.map(details => details.map(s => s.toString().padEnd(20)).join('|'))
			.subscribe(console.log);
	})
	.parse(process.argv);